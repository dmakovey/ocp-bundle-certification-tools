FROM registry.access.redhat.com/ubi8/ubi-minimal as build
ARG OPENSHIFT_VERSION
ARG TEKTON_CLI_VERSION

RUN curl -o /etc/yum.repos.d/gh-cli.repo https://cli.github.com/packages/rpm/gh-cli.repo 
RUN microdnf install -y gh git tar gzip
WORKDIR /tmp
RUN echo https://github.com/tektoncd/cli/releases/download/v${TEKTON_CLI_VERSION}/tkn_${TEKTON_CLI_VERSION}_Linux_x86_64.tar.gz
RUN curl -L https://github.com/tektoncd/cli/releases/download/v${TEKTON_CLI_VERSION}/tkn_${TEKTON_CLI_VERSION}_Linux_x86_64.tar.gz \
    | tar xzf - \
    && mv /tmp/tkn /usr/local/bin/tkn \
    && chown root:root /usr/local/bin/tkn

RUN curl -L "https://mirror.openshift.com/pub/openshift-v4/clients/ocp/${OPENSHIFT_VERSION}/openshift-client-linux.tar.gz" \
    | tar -xzf - \
    && mv /tmp/oc /usr/local/bin/oc

RUN rm -rf /tmp/*

FROM scratch

COPY --from=build / / 
CMD [ "/bin/bash" ] 
